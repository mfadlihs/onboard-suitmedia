type PokeNameUrl = { name: string; url: string }

type Ability = {
  ability: {
    name: string
    url: string
  }
  is_hidden: boolean
  slot: number
}

type Cries = {
  latest: string
  legacy: string
}

type HeldItem = {
  item: PokeNameUrl
  version_details: {
    rarity: number
    version: {
      name: string
      url: string
    }
  }[]
}

type Moves = {
  move: PokeNameUrl
  version_group_details: {
    level_learned_at: 1
    move_learn_method: PokeNameUrl
    version_group: PokeNameUrl
  }[]
}

type Sprites = {
  back_default?: string
  back_female?: string
  back_shiny?: string
  back_shiny_female?: string
  front_default?: string
  front_shiny?: string
  front_shiny_female?: string
  other: {
    dream_world: {
      front_default?: string
      front_female?: string
    }
    home: {
      front_default?: string
      front_female?: string
      front_shiny?: string
      front_shiny_female?: string
    }
    'official-artwork': {
      front_default?: string
      front_shiny?: string
    }
    showdown: {
      back_default?: string
      back_female?: string
      back_shiny?: string
      back_shiny_female?: string
      front_default?: string
      front_female?: string
      front_shiny?: string
      front_shiny_female?: string
    }
  }
}

type Stats = {
  base_stat: number
  effort: number
  stat: PokeNameUrl
}

type PokeTypes = {
  slot: number
  type: PokeNameUrl
}

export interface Pokemon {
  abilities: Ability[]
  base_experience: number
  cries: Cries
  form: PokeNameUrl[]
  game_indices: {
    game_index: number
    version: PokeNameUrl
  }[]
  height: number
  held_items: HeldItem[]
  id: number
  is_default: boolean
  location_area_encounters: string
  moves: Moves
  name: string
  order: number
  past_abilities?: []
  past_types?: []
  species: PokeNameUrl
  sprites: Sprites
  stats: Stats[]
  types: PokeTypes[]
  weight: number
}
