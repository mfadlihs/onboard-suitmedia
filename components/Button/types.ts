type ButtonColor =
  | 'primary'
  | 'secondary'
  | 'warning'
  | 'danger'
  | 'black'
  | 'light-grey'
  | 'white'

type ButtonVariant = 'contained' | 'outlined' | 'ghost'

type ButtonSize = 'small' | 'normal' | 'large'

type ButtonAs = 'link' | 'button'

export interface ButtonProps {
  className?: string
  variant?: ButtonVariant
  color?: ButtonColor
  size?: ButtonSize
  as?: ButtonAs
  onClick?: () => void
  url?: string
  id?: string
  type?: 'button' | 'submit' | 'reset'
  disabled?: boolean
}
