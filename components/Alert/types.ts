type AlertVariant =
  | 'primary'
  | 'secondary'
  | 'warning'
  | 'danger'
  | 'black'
  | 'light-grey'
  | 'white'

export interface AlertProps {
  type?: AlertVariant
  className?: string
  isOpen: boolean
}
