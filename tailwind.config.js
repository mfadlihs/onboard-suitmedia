/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  theme: {
    extend: {
      colors: {
        suitmedia: '#ff6700',
        primary: '#0094ed',
        secondary: '#76848C',
        warning: '#FFAE00',
        danger: '#F0210B',
        'light-grey': '#D3D3D3',
        'white-reversed': '#e8e8e8',
        black: '#000000',
        black90: '#1a1a1a',
        black80: '#333333',
        black70: '#4d4d4d',
        black60: '#666666',
        black50: 'gray',
        black40: '#999999',
        black30: '#b3b3b3',
        black20: '#cccccc',
        black10: '#e6e6e6'
      },
      fontFamily: {
        fira: "font-family: 'Fira-Mono', monospace;"
      }
    }
  },
  plugins: []
}
